FROM human-proof/api:#(BASE_TAG)

# Install the dev system dependencies
RUN apt-get -y -y update -qq
RUN apt-get -y install git
RUN apt-get -y install npm
RUN apt-get -y install vim
RUN apt-get -y install curl
RUN apt-get -y install sudo
RUN apt-get -y install awscli
RUN apt-get -y install python-nose

# Install the dev python dependencies
RUN pip install flake8
RUN apt-get install -y python3-flake8
RUN pip install user_agents
RUN pip install mock
RUN pip install coverage

# Copy the source for testing
COPY api /human-proof/api
COPY config /human-proof/config

# Do stuff as whatever user created the docker environment. This will prevent
# you trashing your repo with stuff owned by root. Nothing is cached passed this point - so make sure this is last!
ARG USER_ID
ARG USER_NAME
ARG GROUP_ID
ARG GROUP_NAME
RUN groupadd -og $GROUP_ID $GROUP_NAME || echo "Group already exists - no worries"
RUN useradd -mu ${USER_ID} -g ${GROUP_ID} ${USER_NAME} || echo "User already exists - no worries"
RUN echo "ALL ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

EXPOSE 8982
EXPOSE 8983

USER $USER_NAME

CMD /bin/bash
