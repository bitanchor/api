#!/bin/bash

# @blame R. Matt McCann <mccann.matt@gmail.com>
# @brief Launches the user into the developer container
# @copyright &copy; 2017 Human-Proof Corp.

docker run --rm -p 8982:8982 -w /human-proof -itv $PWD:/human-proof human-proof/api:dev
