#!/bin/bash

# @blame R. Matt McCann <mccann.matt@gmail.com>
# @brief Builds the developer container
# @copyright &copy; 2017 Human-Proof Corp.

group_id=`id -rg`
group_name=`id -rgn`
user_id=`id -ru`
user_name=`id -run`

cp docker/Dockerfile.base ./Dockerfile
docker build -t human-proof/api:latest \
    --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    . || (\
    echo "*************************************************************************************" && \
    echo "* Docker base build failed!" && \
    echo "*************************************************************************************")

cp docker/Dockerfile.dev ./Dockerfile
sed -i 's/#(BASE_TAG)/latest/g' ./Dockerfile
docker build -t human-proof/api:dev \
    --build-arg GROUP_ID=$group_id \
    --build-arg GROUP_NAME=$group_name \
    --build-arg USER_ID=$user_id \
    --build-arg USER_NAME=$user_name \
    . || echo "docker dev build failed"
rm Dockerfile
