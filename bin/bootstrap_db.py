#!/usr/bin/env python

import os
import sys

sys.path.insert(0, './api')
sys.path.insert(0, './external')

from human_proof.api.user import User # noqa
from human_proof.api.file_access import FileAccess # noqa
from human_proof.api.file import File # noqa

if 'TARGET' not in os.environ.keys():
    print("TARGET not defined in ENV; Defaulting to test")
    os.environ['TARGET'] = 'test'

if os.environ['TARGET'] not in ['test', 'production']:
    raise ValueError("TARGET in ENV must be either 'test' or 'production'")

FileAccess.db_bootstrap()
File.db_bootstrap()
User.db_bootstrap()
