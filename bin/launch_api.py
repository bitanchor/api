#!/usr/bin/env python

import sys
sys.path.insert(0, './api')
sys.path.insert(0, './external')

import os # noqa

from flask_cors import CORS # noqa

os.environ['TARGET'] = 'test'

from human_proof.api.api_root import api # noqa

# Launch the api
CORS(api)
api.run(host='0.0.0.0', port=8982)
