import sys
sys.path.insert(0, '/var/www')
sys.path.insert(0, '/var/www/api')
sys.path.insert(0, '/var/www/external')

import os
os.environ['TARGET'] = 'production'

from evil_corp.api.api_root import api as application

from flask_cors import CORS

CORS(
    application,
    resources={r"/*": {"origins": "*"}}
)
