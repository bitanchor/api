#!/usr/bin/env python3

import boto3
import sys

client = boto3.client('ecs')

if len(sys.argv) != 2:
    print("Usage: %s [cluster_name]" % sys.argv[0])
    sys.exit(-1)

cluster = sys.argv[1]

# Iterate over all of the tasks and kill them
next_token = 'NotNone!'
while next_token is not None:
    response = client.list_tasks(cluster=cluster, desiredStatus='RUNNING')
    print(response)

    for task in response['taskArns']:
        print("Stopping task %s" % task)
        client.stop_task(cluster=cluster, task=task, reason='update')

    if 'nextToken' in response:
        next_token = response['nextToken']
    else:
        next_token = None

print("Finished stopping tasks!")