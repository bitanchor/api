'''
@author R. Matt McCann
@brief Model & API for files tracked by the system.
@copyright &copy; 2017 Human-Proof Corp.
'''

import flask
import os

import dynoflrud

from human_proof.api.api_root import api
from human_proof.api.file_access import FileAccess


class File(dynoflrud.Record):
    ''' Record representing a file tracked by the system. '''

    ''' Fields required to commit the record to the db. '''
    DB_FIELDS_REQUIRED = ['context', 'name', 'is_locked']

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HumanProof.File' % os.environ['TARGET']


@api.route('/file/', methods=['POST'])
def file_create():
    return dynoflrud.create(File)


@api.route('/file/<id>/list_file_access/', methods=['POST'])
def file_list_file_access(id):
    ''' List the file's access history. '''
    # TODO(mmccann): Support fetching more than the first page
    results = FileAccess.db_list_all(limit=100, file_id=id)

    # Encode the results
    encoded = []
    for result in results:
        encoded.append(dynoflrud.jsonify(result, flaskify=False))

    return flask.jsonify(encoded)


@api.route('/file/<id>/', methods=['GET'])
def file_read(id):
    return dynoflrud.jsonify(File.db_get(id=id))


@api.route('/file/<id>/', methods=['PUT'])
def file_update(id):
    return dynoflrud.update(File, id)
