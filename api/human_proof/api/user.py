'''
@author R. Matt McCann
@brief Model & API for end users
@copyright &copy; 2017 Human-Proof Corp.
'''

import flask
import os

import dynoflrud

from human_proof.api.api_root import api
from human_proof.api.file_access import FileAccess


class User(dynoflrud.Record):
    ''' Record representing an end user who consumes files. '''

    ''' Fields required to commit the record to the db. '''
    DB_FIELDS_REQUIRED = ['id']

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HumanProof.User' % os.environ['TARGET']


@api.route('/user/', methods=['POST'])
def user_create():
    return dynoflrud.create(User)


@api.route('/user/<id>/list_file_access/', methods=['POST'])
def user_list_file_access(id):
    ''' List the user's file access history. '''
    # TODO(mmccann): Support fetching more than the first page
    results = FileAccess.db_list_all(limit=100, user_id=id)

    # Encode the results
    encoded = []
    for result in results:
        encoded.append(dynoflrud.jsonify(result, flaskify=False))

    return flask.jsonify(encoded)


@api.route('/user/<id>/', methods=['GET'])
def user_read(id):
    return dynoflrud.jsonify(User.db_get(id=id))


@api.route('/user/<id>/', methods=['PUT'])
def user_update(id):
    return dynoflrud.update(User, id)
