'''
@author R. Matt McCann
@brief API for parsing audio samples
@copyright& copy; 2017 Evil Corp.
'''

import flask
import os

from uuid import uuid4

from human_proof.api.api_root import api
#from libreceiver import Beacon, WavFile


#@api.route('/audio_sample/parse/', methods=['POST'])
#def audio_sample_parse():
#    ''' Parses the provided audio sample and returns whether or not the listener is within the location context. '''
#    file = flask.request.files['file']

#    # Stash the file locally (HACK)
#    file_name = '/tmp/%s.wav' % uuid4()
#    file.save(file_name)

#    # If the wav file is not at the desired sampling rate
#    wav_file = WavFile.create(file_name)
#    if wav_file.sampling_frequency() != 41992:
#        # Resample the file to ensure the desired sampling rate
#        resampled_file_name = '%s.resampled.wav' % file_name
#        WavFile.resample(file_name, resampled_file_name, 41992)

#        # Switch in the resampled version
#        os.system('mv %s %s' % (resampled_file_name, file_name))

#    # Parse the audio sample
#    beacon = Beacon.create(file_name)

#    # Delete the wav file
#    os.system('rm %s' % file_name)

#    # Return the in location result
#    response = flask.jsonify({
#        'is_in_location': beacon.is_in_location(),
#        'hamming_distance': beacon.hamming_distance(),
#        'hamming_distance_key': beacon.hamming_distance_key()
#    })

#    return response
