'''
@author R. Matt McCann
@brief Unit tests for the audio sample api
@copyright &copy; 2017 R. Matt McCann
'''

import json

from io import BytesIO

from human_proof.api.api_root import api


class TestAudioSampleApi(object):
    ''' Unit tests for the audio sample api. '''

    def setup(self):
        ''' Setup before each test. '''
        self.api = api.test_client()

    def test_parse(self):
        ''' Simple does-it-blow-up test. '''
        with open('api/human-proof/api/test/key.wav', 'rb') as wav_file:
            file = (BytesIO(wav_file.read()), "key.wav")
        data = dict(file=file,)

        # Upload the wav file
        response = self.api.post(
            '/audio_sample/parse/', content_type='multipart/form-data', data=data
        )
        decoded = json.loads(response.get_data(as_text=True))

        # Check the response
        assert response.status_code == 200, response.status_code
        assert decoded['is_in_location'] is True, decoded['is_in_location']
        assert decoded['hamming_distance'] == 0.0, decoded['hamming_distance']
