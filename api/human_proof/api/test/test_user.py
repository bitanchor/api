'''
@author R. Matt McCann
@brief Unit tests for the User model and its api's
@copyright &copy; 2017 Human-Proof Corp.
'''

import json

from human_proof.api.file_access import FileAccess
from human_proof.api.user import User
from human_proof.api.api_root import api


class TestUserApi(object):
    ''' Unit tests for the User API. '''

    def setup(self):
        ''' Setup before each test. '''
        # TODO(mmccann): This prevents parallel execution!
        User.db_debootstrap()
        User.db_bootstrap()
        FileAccess.db_debootstrap()
        FileAccess.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

    def test_list_file_access(self):
        ''' Tests lists the file accessing for a user. '''
        for iter in range(12):
            file_access = FileAccess.construct()
            file_access.action = 'read'
            file_access.context = 'nationwide_vpn'
            file_access.file_name = 'filename.doc'
            file_access.location_lat = 35.35
            file_access.location_lon = 33.44
            file_access.was_permitted = True
            file_access.time = str(iter)
            file_access.file_id = str(iter)
            file_access.user_id = '0' if iter < 10 else '1'
            file_access.db_create()

        response = self.api.post('/user/0/list_file_access/')
        print "Response", response
        decoded = json.loads(response.get_data(as_text=True))

        assert len(decoded) == 10, decoded
        for record in decoded:
            assert record['user_id'] == '0', record
