'''
@author R. Matt McCann
@brief Unit tests for the FileAccess model and its api's
@copyright &copy; 2017 Human-Proof Corp.
'''

import json
import mock

from human_proof.api.file_access import FileAccess
from human_proof.api.api_root import api


class TestFileAccessApi(object):
    ''' Unit tests for the FileAcess API. '''

    def setup(self):
        ''' Setup before each test. '''
        # TODO(mmccann): This prevents parallel execution!
        FileAccess.db_debootstrap()
        FileAccess.db_bootstrap()

        api.config['TESTING'] = True

        self.api = api.test_client()

    def test_create__good_path(self):
        ''' Simple happy path test for creating FileAccess records. '''
        data = {
            'action': 'read',
            'context': 'nationwide_vpn',
            'file_name': 'filename.doc',
            'location_lat': 35.35235,
            'location_lon': 24.054955,
            'was_permitted': True,
            'time': '13/12/11 23:50:11',
            'file_id': 'file_id',
            'user_id': 'user_id'
        }

        # Create the record
        response = self.api.post(
            '/file/access/', data=json.dumps(data), content_type='application/json'
        )

        # Check the response
        decoded = json.loads(response.get_data(as_text=True))
        assert 'id' in decoded, decoded
        assert decoded['action'] == data['action'], decoded
        assert decoded['context'] == data['context'], decoded
        assert decoded['file_name'] == data['file_name'], decoded
        assert decoded['location_lat'] == data['location_lat'], decoded
        assert decoded['location_lon'] == data['location_lon'], decoded
        assert decoded['was_permitted'] == data['was_permitted'], decoded
        assert decoded['time'] == data['time'], decoded

        # Check that the record was created properly
        file_access = FileAccess.db_get(id=decoded['id'])
        assert file_access.id == decoded['id'], decoded
        assert file_access.action == decoded['action'], decoded
        assert file_access.context == decoded['context'], decoded
        assert file_access.file_name == decoded['file_name'], decoded
        assert file_access.location_lat == decoded['location_lat'], decoded
        assert file_access.location_lon == decoded['location_lon'], decoded
        assert file_access.was_permitted == decoded['was_permitted'], decoded
        assert file_access.time == decoded['time'], decoded