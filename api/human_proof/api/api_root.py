'''
@blame R. Matt McCann <mccann.matt@gmail.com>
@brief Configures the Flask-powered API
@copyright &copy; 2017 Evil Corp.
'''

from flask import Flask

# Initialize the Flask module
api = Flask(__name__)

# Prevent big file uploads
api.config['MAX_CONTENT_LENGTH'] = 1024 * 1024 * 1024

# Import our APIs
import human_proof.api.audio_sample # NOQA
import human_proof.api.file_access # NOQA
import human_proof.api.file # NOQA
import human_proof.api.user # NOQA
