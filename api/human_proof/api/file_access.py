'''
@author R. Matt McCann
@brief Model & API for file access records
@copyright &copy; 2017 Human-Proof Corp.
'''

import os

import dynoflrud

from human_proof.api.api_root import api


class FileAccess(dynoflrud.Record):
    ''' Record representing a file being accessed by a user. '''

    ''' Attribute definitions for the composite key. '''
    DB_TABLE_ATTRIBUTE_DEFINITIONS = [
        {'AttributeName': 'user_id', 'AttributeType': 'S'}, {'AttributeName': 'time', 'AttributeType': 'S'}
    ]

    ''' Composite fields comprising the record key. '''
    DB_TABLE_KEY_SCHEMA = [
        {'AttributeName': 'user_id', 'KeyType': 'HASH'}, {'AttributeName': 'time', 'KeyType': 'RANGE'}
    ]

    ''' Fields required to commit the record to the db. '''
    DB_FIELDS_REQUIRED = [
        'action', 'context', 'file_name', 'location_lat', 'location_lon', 'was_permitted', 'time', 'user_id', 'file_id'
    ]

    ''' Table name for this model. '''
    DB_TABLE_NAME = '%s.HumanProof.FileAccess' % os.environ['TARGET']


@api.route('/file/access/', methods=['POST'])
def file_access_create():
    # TODO: Check inputs

    return dynoflrud.create(FileAccess)


@api.route('/file/access/<id>/', methods=['GET'])
def file_access_read(id):
    return dynoflrud.jsonify(FileAccess.db_get(id=id))


@api.route('/file/access/<id>/', methods=['PUT'])
def file_access_update(id):
    return dynoflrud.update(FileAccess, id)
