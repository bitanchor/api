import os
import os.path
import sys

sys.path.insert(0, '.')
sys.path.insert(0, './external')

if 'TARGET' not in os.environ.keys():
    print("TARGET not defined in ENV; Defaulting to 'test'")
    os.environ['TARGET'] = 'test'

if os.environ['TARGET'] not in ['test', 'production']:
    raise ValueError("TARGET must be defined in ENV as 'test' or 'production'")

path_to_this_file = os.path.dirname(__file__)
if os.environ['TARGET'] == 'test':
    os.environ['TARGET_CONFIG_FILE'] = '%s/../../config/test.conf' % path_to_this_file
elif os.environ['TARGET'] == 'production':
    os.environ['TARGET_CONFIG_FILE'] = '%s/../../config/production.conf' % path_to_this_file
